//
//  ViewController.swift
//  Latihan1
//
//  Created by Macbook Pro on 17/03/23.
//

import UIKit

class ViewController: UIViewController {
    
    

    @IBOutlet weak var astronoutButton:
    UIButton!
    @IBOutlet weak var rockStarButton:
    UIButton!
    @IBOutlet weak var engineerButtton:
    UIButton!
    @IBOutlet weak var rocketButton:
    UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func showAlert(with title: String, subtitle: String){
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        
        present(alert, animated: true)
        
    }

    @IBAction func astronoutButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", subtitle: "Astronout")
    }
    
    @IBAction func rockStarButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", subtitle: "RockStar")
    }
    
    
    @IBAction func engineerButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", subtitle: "Engineering")
    }
    
    
    @IBAction func rocketButtonTapped(_ sender: Any) {
        showAlert(with: "Your Job", subtitle: "NASA")
    }
}

