import UIKit

var greeting = "Hello, playground"

//variables
var accountName = "kopisurgawi"
var followers = 363
print(followers)

followers = 689

//kalo var bisa diubah, tapi kalo yang let gabisa diubah

//2 cara untuk ngeprint
print(accountName + " followersnya", followers)
print("\(accountName) followersnya \(followers)")

//constant
let location = "Bandung"
print(location)

//tipe data string yaitu data yang berisi sekumpulan karakter
let contactPerson = "08788934532"
var userCategory = "personal Blogger"

//tipe data int, data kumpulan bilangan bulat.

var following = 356

//tipe data couble. kumpulan bilangan desimal dimana double lebih presisi daripada float

var percentageFollowing = 0.3

//boolean
var isTiktok = true

//1. Assignment operator
var item = 7
var another = item
print(item)
print(another)

//2. Arithmetic operator
var a = 2
var b = 4
var c = 6
var d = 3

print(a+b)
print(c-b)
print(a*b*c)
print(b/a)
print(b%d)
print(b%a)

//3. compound assignment operator
var age = 17
//age = age + 1
age += 1

//4. comparison operator

let aNumber = 1
let bNumber = 2

aNumber == bNumber
aNumber != bNumber
aNumber > bNumber
aNumber <= bNumber

//5. Logical operator
//ada dua macam logical operator
//!, atau NOT atau disebut juga unary operator
//&&, atau AND disebut juga binary operator
//|| atau OR sama juga disebutnya binary operator
let isTiktokActive = true
let isTwitterActive = false

if !isTiktokActive {
    print("Gak punya akun twitter")
}else{
    print("punya akun tiktok")
}

isTiktokActive && isTwitterActive
isTiktokActive || isTwitterActive

//IF statement
let userReels = 9

if userReels < 10 {
    print("He is not influencer because Reels is less than 10")
}

//nested if statements

var rateCard = 5000

if rateCard >= 10000{
    print("He/she is macro influencer")
}else if rateCard >= 5000 && rateCard < 10000 {
    print("He/she is on-budget influencer")
}else if rateCard >= 10000 && rateCard < 5000 {
    print ("He/she is micro influencer")
}else {
    print("He/she is inexperienced influencer")
}

//ada solusi yang lebih elegan dari kondisi pencabangan diatas
//bisa pake switch statements

switch rateCard{
case 10000...:
    print("He/she is macro influencer")
case 5000...9999:
    print("He/she is on-budget influencer")
case 1000...4999:
    print("He/she is micro influencer")
default:
    print("He/she is inexperienced influencer")
}




let value = 1

switch value {
case 1,2,3:
    print("Lorem ipsum")
default:
    print("default")
}



